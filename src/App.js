import './App.css';
// import BakeryCard from './components/BakeryCard';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Data from './data.json';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import SearchAppBar from './components/AppBar';
import CircleIcon from '@mui/icons-material/Circle';


function App() {
  return (
    <div className="App">
      <SearchAppBar />
      <Container>
        <Box marginTop={10}sx={{
          display: "flex"
        }}>
          <h6>Place List</h6>
        </Box>
        <Grid container spacing={3}>
          {
            Data.map( data => {
              return(
                <Grid item xs={4}>
                  <Paper elevation={2}>
                      <Box padding={2}
                          sx={{
                              display: "flex",
                              alignItems: "center"
                          }}
                          marginTop={1}
                      >
                          <img class="img-profile" src={ data.profile_image_url } alt="" />
                          <Box marginLeft={1}>
                              <h6>{ data.name }</h6>
                              <Box sx={{
                                  display: "flex",
                                  alignItems: "flex-end"
                              }}
                              marginTop={1}
                              >
                                  <CalendarMonthIcon sx={{width: 18}} />
                                  {/* { data.operation_time.map( data => {
                                    return(
                                      <span class="fs-14">
                                        { data.time_open }
                                      </span>
                                    )
                                  }) } */}
                                  <span class="fs-14">9.00-20.00</span>
                                  <CircleIcon sx={{
                                    width: 14,
                                    marginLeft: 16,
                                    marginRight: 1
                                    }} color="primary" />
                                  <span>{ data.rating }</span>
                              </Box>
                          </Box>
                      </Box>
                      <Box padding={2}>
                        <img class="img-show0" src={ data.images['0'] } alt="" />
                        <img class="img-show1" src={ data.images['1'] } alt="" />
                        <img class="img-show2" src={ data.images['2'] } alt="" />
                      </Box>
                  </Paper>
              </Grid>
                // <BakeryCard />
              )
            })
          }
        </Grid>
      </Container>
    </div>
  );
}

export default App;
